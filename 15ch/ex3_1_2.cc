/*
    从派生类对象到基类对象的转换
        派生类对象中含有基类对象，转换的时候将派生类的基类部分赋值给基类对象
    从基类到派生类对象的转换:
        基类没有包含派生类中的成员，所以不能转换到派生类对象

 */
#include <iostream>

using namespace std;

class A{
public:
    void printA(){cout << "A\n";}
};
class B:public A{
public:
    void printB(){cout << "B\n";}
};
int main()
{
    A a=A();
    B b=B();
    A a2;
    B b2;
    a.printA();
    b.printA();
    b.printB();

    //从派生类对象到基类对象的转换
    a2 = b;
    a2.printA();

/*
    //从基类到派生类对象的转换
    b2 = a;
    b2.printA();
*/

/*
   //不能将基类指针或者引用转换为派生类指针或者引用
    A * pA;
    B * pB;
    pB = pA;
*/
}
