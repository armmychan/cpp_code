/*
   派生类只能通过派生类对象访问其基类的protected成员，派生类对其基类类型对象的protected成员没有特殊访问权限
 */
#include <iostream>

using namespace std;
class Item_base{
public:
    Item_base(string &book = "", double salse = 0.0):isbn(book),price(salse){}
    string book(){
        return isbn;
    }

    virtual double net_price(size_t n){
        return n * price;
    }
    virtual ~Item_base(){}
protected:
    double price;
private:
    string isbn;
};

class Bulk_item :public Item_base{
public:
    Bulk_item(string &book = "", double salse = 0.0):Item_base(book, salse){}

    //通过this对象访问本基类的protected成员
    void menfcn(Item_base &b)
    {
        double ret = 0;
        ret = this->price;

        //派生类只能通过派生类对象访问其基类的protected成员，派生类对其基类类型对象的protected成员没有特殊访问权限
        //ret = b.price;
    }

    //通过本类类型的其他对象访问基类的protected成员
    void menfcn(Bulk_item &d, Item_base &b)
    {
        double ret = price;
        ret = d.price;
        //派生类只能通过派生类对象访问其基类的protected成员，派生类对其基类类型对象的protected成员没有特殊访问权限
        //ret = b.price;
    }

};
int main()
{
}
