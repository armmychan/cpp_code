/*
   继承与静态成员：派生类所继承的static成员与基类为同一实例
 */
#include <iostream>

using namespace std;

class A{
public:
    static int i;
};
int A::i = 10;
class B :public A{};
int main(){
    A a = A();
    //基类对象中static输出10；
    cout << a.i << endl;

    B b = B();
    //派生类对象中的static增加1
    cout << ++b.i << endl;  //输出 11
    //再次输出派生类中的static成员为 11
    cout << a.i << endl;    //输出 11
}
