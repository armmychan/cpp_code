/*
   定义一个基类
 */
class Item_base{
public:
    Item_base(const std::string &book = "", double sales_price = 0.0):isbn(book),price(sales_price){}

    std::string book() const {
        return isbn;
    }

    //保留字virtual的目的是启动动态绑定，成员默认为非虚函数
    //除了构造函数之外，任意非static成员函数都可以定义为虚函数
    //保留字只在类内部成员函数声明中出现，不能用在类定义体外部出现的函数定义上
    //基类通常将派生类需要重写的函数定义为虚函数
    virtual double net_price(std::size_t n)
    {
        return n * price;
    }

    virtual ~Item_base(){}
private:
    std::string isbn;
protected:
    double price;
};
