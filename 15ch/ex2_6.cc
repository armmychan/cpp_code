/*
   友元关系不能继承，如果需要友元，在派生类中必须显式声明
 */
#include <iostream>

using namespace std;

class Base{
    friend class Frnd;
protected:
    int i;
};
class D1 :public Base{
protected:
    int j;
};
class Frnd{
public:
    int men(Base b){
        return b.i;
    }
    
/*
    //错误，因为D1没有继承Base中对Frnd的友元关系
    int men(D1 d){
        return d.j;
    }
*/
};
class D2 :public Frnd{
public:
/*
   //错误，因为D2没有继承Base中对Frnd的友元关系
    int men(Base b){
        return b.i;
    }
*/
};
int main()
{
}
