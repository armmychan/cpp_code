#include <iostream>

using namespace std;

class Item_base
{
    string isbn;
protected:
    double price;
public:
    Item_base(const char * isbn = "", double price = 0):isbn(isbn),price(price){
        cout << "Item_base_begin\n";
    }
    string book()
    {
        return isbn;
    }

    virtual double net_price( int n){
       return n * price; 
    }
    double get_price()
    {
        return price;
    }
    virtual ~Item_base()
    {
        cout << "Item_base_end\n";
    }
};

class Bult_item :public Item_base
{
public:
    Bult_item(const char * isbn = "", double price = 0):Item_base(isbn, price){
        cout << "Bult_item_begin\n";
    }
    double net_price(int n)
    {
        return n * price * 0.8;
    }
    ~Bult_item(){
        cout << "Bult_item_end\n";
    }
};

int main()
{
    Item_base itb = Item_base();
    Bult_item bit = Bult_item();
}
