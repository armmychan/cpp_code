#include <iostream>

using namespace std;

class A{
protected:
    string name;
public:
    A(string str = ""): name(str){}
    void printA() const {
        cout << name << endl;;
    }
};
class B :public A{
public:
    B(string str = ""): A(str){}
};
int main()
{
    A("AAA").printA();
    B("BBB").printA();
}
