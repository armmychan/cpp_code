/*
	指针加减要乘以本身的大小,比如int *p = 1000;p = p + 2;结果是1000 + 2 *sizeof(int) = 1008
		&stu.i + sizeof(int)等于&stu.i + 4 *sizeof(int)
		&stu + sizeof(int)等于&stu + 4 * sizeof(stu) 
		sizeof(int)是4 所以结果会多16 16进制是0x10
		sizeof(stu)是8 所以结果会多32 16进制是0x20
 
*/

#include <iostream>

using namespace std;
struct Student{
    int i;
    char ch;
};
int main()
{
    struct Student stu = {10, 'a'};
    
    cout << "stu:" << &stu << endl;				//0x******70
    cout << "int i:" << &stu.i + sizeof(int) << endl;			//0x******80  = 0x*****70 + 0x10
    cout << "char ch:" << &stu + sizeof(int) << endl;		//0x******90 = 0x******70 + 0x20
}

