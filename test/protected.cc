/*
   理解：protected成员可以被派生类对象使用，但不可以呗该类型的普通类型使用：
    protected成员可以被派生类对象使用：
        意思是在派生类定义体中通过派生类对象使用；
    但不可以被该类型的普通类型使用：
        意思是不在该类或者该类的派生类中，不能使用protected对象;

 */
#include <iostream>

using namespace std;

class A{
protected:
    int i;
    void printA(){
        cout << "AAA\n"; 
    }
};
class B :public A{
public:
    int i;
    void printInt(int n){
        i = n;
        cout << "i = " << i << endl;
        cout << "A::i = " << A::i << endl;
    }
};
int main()
{
    B().printInt(10);
}
