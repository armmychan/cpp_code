/*
 * 在容器中添加一段元素
 * */
#include <iostream>
#include <vector>
#include <list>
#include <deque>

using namespace std;
int main()
{
	vector<string> vstr;
	list<string> lstr;
	deque<string> dstr;
	string str = "inste_string";

	vstr.insert(vstr.begin(), 10, str);
	for(vector<string>::iterator it = vstr.begin(); it != vstr.end(); ++it)
		cout << *it << ' ';
	cout << endl;

	lstr.insert(lstr.begin(), 10, str);
	for(list<string>::iterator it = lstr.begin(); it != lstr.end(); ++it)
		cout << *it << ' ';
	cout << endl;

	dstr.insert(dstr.begin(), 10, str);
	for(deque<string>::iterator it = dstr.begin(); it != dstr.end(); ++it)
		cout << *it << ' ';
	cout << endl;
}

