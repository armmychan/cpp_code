/*
 * 在容器中添加指定段的元素
 * */
#include <iostream>
#include <vector>
#include <list>
#include <deque>

using namespace std;
int main()
{
	vector<string> vstr;
	list<string> lstr;
	deque<string> dstr;
	string strs[] = {"1st", "2nd", "3rd", "4th"};

	vstr.insert(vstr.begin(), strs, strs + 4);
	for(vector<string>::iterator it = vstr.begin(); it != vstr.end(); ++it)
		cout << *it << ' ';
	cout << endl;

	lstr.insert(lstr.begin(), strs, strs + 4);
	for(list<string>::iterator it = lstr.begin(); it != lstr.end(); ++it)
		cout << *it << ' ';
	cout << endl;

	dstr.insert(dstr.begin(), strs, strs + 4);
	for(deque<string>::iterator it = dstr.begin(); it != dstr.end(); ++it)
		cout << *it << ' ';
	cout << endl;

	vector<string>::iterator vit = vstr.begin();
	while(vit != vstr.end())
	{
		vstr.insert(++vit, "fuck");
		//++vit;
	}
	for(vector<string>::iterator it = vstr.begin(); it != vstr.end(); ++it)
		cout << *it << ' ';
	cout << endl;
}

