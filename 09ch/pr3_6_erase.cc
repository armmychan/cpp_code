#include <iostream>
#include <deque>

using namespace std;
int main()
{
	string strs[] = {"1st", "2en", "3th", "4th"};
	deque<string> dst(strs, strs + 4); 

	for(deque<string>::iterator it = dst.begin(); it != dst.end(); ++it)
		cout << *it << " ";
	cout << endl;
	
	dst.pop_back();
	cout << "it.poo_back()\n";
	for(deque<string>::iterator it = dst.begin(); it != dst.end(); ++it)
		cout << *it << " ";
	cout << endl;
	
	dst.pop_front();
	cout << "it.pop_front()\n";
	for(deque<string>::iterator it = dst.begin(); it != dst.end(); ++it)
		cout << *it << " ";
	cout << endl;
	
	dst.clear();
	cout << "it.pop_clear()\n";
	for(deque<string>::iterator it = dst.begin(); it != dst.end(); ++it)
		cout << *it << " ";
	cout << endl;

}
