#include <iostream>
#include <algorithm>
#include <deque>

using namespace std;
int main()
{
	string strs[] = {"1st", "2en", "3th", "4th"};
	deque<string> dst(strs, strs + 4); 

	cout << "find and delete 4th\n";
	deque<string>::iterator it = find(dst.begin(), dst.end(), "4th");
	cout << *it << endl;
	if(it != dst.end())
		dst.erase(it);
	for(deque<string>::iterator it = dst.begin(); it != dst.end(); ++ it)
		cout << *it << ' ';
	cout << endl;

	cout << "find and delete 5th\n";
	it = find(dst.begin(), dst.end(), "5th");
	cout << *it << endl;
	if(it != dst.end())
		dst.erase(it);
	for(deque<string>::iterator it = dst.begin(); it != dst.end(); ++ it)
		cout << *it << ' ';
	cout << endl;
}

