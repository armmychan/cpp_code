/*
 * 指针就是跌大气，因此允许通过使用内置数组中一堆指针初始化容器
 * */
#include <iostream>
#include <vector>
#include <list>

using namespace std;

int main()
{
	char * words[] = {"stately", "plump", "buck", "mulligan"};
	size_t words_size = sizeof(words) / sizeof(char *);
	cout << words_size << endl;
	cout << *words << endl;
	cout << *(words + 1) << endl;
	list<string> words2(words,words + words_size);
	words2[1];
}
