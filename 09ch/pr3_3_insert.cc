/*
 * 在容器中的指定位置添加元素
 * */
#include <iostream>
#include <vector>
#include <deque>
#include <list>

using namespace std;
int main()
{
	vector<string> vst;
	list<string> lst;
	deque<string> dst;
	string str("insert_value");

	vst.insert(vst.begin(), str);
	cout << *vst.begin() << endl;

	lst.insert(lst.begin(), str);
	cout << *lst.begin() << endl;

	dst.insert(dst.begin(), str);
	cout << *dst.begin() << endl;
}

