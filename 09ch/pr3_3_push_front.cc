#include <iostream>
#include <list>

using namespace std;
int main()
{
	list<int> vint;

	for(size_t ix = 0; ix != 4; ++ix)
		vint.push_back(ix);

	for(size_t ix = 0; ix != 4; ++ix)
		vint.push_front(ix);
	for(list<int>::iterator it = vint.begin(); it != vint.end(); ++it)
		cout << *it << endl;
}
