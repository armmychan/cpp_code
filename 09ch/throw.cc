#include <iostream>

using namespace std;

class A{
	string s;
public:
	A(const char * s=""):s(s){}
	void showerror(){
		cout << s << endl;
	}
};
int main()
{
	int i = 1;
	try{
	if(i == 1)
		throw A("Error!");
	}catch(string a)
	{
		//a.showerror();
	}catch(...)
	{
		cout << "nothing!" << endl;
	}
}

