#include <iostream>
#include <vector>
#include <algorithm>
#include <iterator>

using namespace std;
int main()
{
    vector<int> ivec(10, 0);
    vector<int> ivec2;

    replace_copy(ivec.begin(), ivec.end(), back_inserter(ivec2),0, 42);

    for(vector<int>::iterator it = ivec2.begin(); it != ivec2.end(); ++it)
        cout << *it << ' ';
    cout << endl;

    replace(ivec.begin(), ivec.end(), 0, 42);

    for(vector<int>::iterator it = ivec.begin(); it != ivec.end(); ++it)
        cout << *it << ' ';
    cout << endl;

}
