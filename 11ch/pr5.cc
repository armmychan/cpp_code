/*
 * find_first_of()在第二个范围内查找第一个范围内的元素，如果找到，则返回该元素的迭代器，否则返回第一个范围的最后一个迭代器
 * */
#include <iostream>
#include <vector>
//#include <numeric>
#include <algorithm>

using namespace std;
int main()
{
    vector<int> vec1;
    vector<int> vec2;
    int cnt = 0;

    for(int i = 0; i < 10; ++i)
        vec1.push_back(i);
    for(int i = 7; i < 20; ++i)
        vec2.push_back(i);
    vector<int>::iterator it = vec1.begin();
    while((it = find_first_of(it, vec1.end(), vec2.begin(), vec2.end())) != vec1.end())
    {
        ++cnt;
        //++it;
    }
    cout << cnt << endl;
}

