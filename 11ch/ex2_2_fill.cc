#include <iostream>
#include <vector>
#include <algorithm>

using namespace std;
int main()
{
    vector<int> ivec(10, 2);

    for(vector<int>::iterator it = ivec.begin(); it != ivec.end(); ++it)
        cout << *it << ' ';
    cout << endl;

    fill(ivec.begin(), ivec.end(), 10);

    for(vector<int>::iterator it = ivec.begin(); it != ivec.end(); ++it)
        cout << *it << ' ';
    cout << endl;

}

