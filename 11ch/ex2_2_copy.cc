#include <iostream>
#include <algorithm>
#include <list>
#include <vector>
#include <iterator>

using namespace std;
int main()
{
    vector<int> ivec;
    list<int> ilist;

    for(int i = 0; i < 10; ++i)
        ilist.push_back(i);

    copy(ilist.begin(), ilist.end(), back_inserter(ivec));

    for(vector<int>::iterator it = ivec.begin(); it != ivec.end(); ++it)
        cout << *it << ' ';
    cout << endl;
}
