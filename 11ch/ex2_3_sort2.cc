#include <iostream>
#include <vector>
#include <algorithm>
#include <fstream>

using namespace std;
bool isShort(string s1, string s2){
    return s1.size() < s2.size();
}

bool GT6(string s)
{
    return s.size() >= 6;
}
int main()
{
    vector<string> svec;
    ifstream gushi("./gushi.txt");

    string str="";
    while(gushi >> str){
        svec.push_back(str);
    }

    for(vector<string>::iterator it = svec.begin(); it != svec.end(); ++it)
        cout << *it << ' ';
    cout << endl;

    //排序, 这个算法使用小于号操作排序
    /*
    sort(svec.begin(), svec.end());

    for(vector<string>::iterator it = svec.begin(); it != svec.end(); ++it)
        cout << *it << ' ';
    cout << endl;
    */

    //删除重复元素
    //unique并没有删除元素，而是将不重复的元素向前移
    //unique返回的是无重复元素的下一位的迭代其
    vector<string>::iterator it = unique(svec.begin(), svec.end());

    for(vector<string>::iterator it = svec.begin(); it != svec.end(); ++it)
        cout << *it << ' ';
    cout << endl;

    //删除重复元素
    //算法不改变容器的元素，如果需要删除增加元素，必须调用容器的方法
    svec.erase(it, svec.end());

    for(vector<string>::iterator it = svec.begin(); it != svec.end(); ++it)
        cout << *it << ' ';
    cout << endl;

    stable_sort(svec.begin(), svec.end(), isShort);

    for(vector<string>::iterator it = svec.begin(); it != svec.end(); ++it)
        cout << *it << ' ';
    cout << endl;

    vector<string>::size_type wc = count_if(svec.begin(), svec.end(), GT6);
    cout << wc << endl;
}

