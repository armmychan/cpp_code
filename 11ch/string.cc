#include <iostream>
#include <cstdlib>
#include <cstdio>

using namespace std;

class SString{
    char * a;
public:
    SString(char * a = NULL):a(a){}
    friend ostream &operator<< (ostream &os, const SString & str);
};

ostream &operator<< (ostream &os, const SString & str){
    os << str.a;
    return os;
}

int main()
{
    SString s = "this is a string object!";
    cout << s << endl;;

}
