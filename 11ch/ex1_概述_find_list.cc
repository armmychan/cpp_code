#include <iostream>
#include <list>
#include <algorithm>

using namespace std;
int main()
{
    list<int> ilist;
    int search_int = 8;

    for(int i = 0; i < 10; i++)
        ilist.push_back(i);
        //ilist.push_front(i);  //OK!

    list<int>::iterator it = find(ilist.begin(), ilist.end(), search_int);

    cout << (it == ilist.end() ? "NOT FIND!":"OK, FIND!") << endl;

    for(list<int>::iterator it = ilist.begin(); it != ilist.end(); ++it)
        cout << *it << ' ';
    cout << endl;
}

