#include <iostream>
#include <vector>
#include <algorithm>

using namespace std;
int main()
{
    vector<int> ivec(10, 2);

    fill_n(ivec.begin(), 4, 10);

    for(vector<int>::iterator it = ivec.begin(); it != ivec.end(); ++it)
        cout << *it << ' ';
    cout << endl;
    cout << ivec.end() - ivec.begin() << endl;

}

