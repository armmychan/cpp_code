#include <iostream>
#include <vector>
#include <algorithm>

using namespace std;
int main()
{
    vector<int> ivec;
    int search_int = 8;

    //为容器赋值
    for(int i = 0; i < 10; ++i)
        ivec.push_back(i);
    vector<int>::iterator it = find(ivec.begin(), ivec.end(), search_int);
    cout << (it == ivec.end() ? "NOT FIND!":"OK,FIND!") << endl;

    for(vector<int>::iterator it = ivec.begin(); it != ivec.end(); ++it)
        cout << *it << ' ';
    cout << endl;
}

