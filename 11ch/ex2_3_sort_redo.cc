#include <iostream>
#include <fstream>
#include <vector>
#include <algorithm>

using namespace std;
bool isShort(string s1, string s2)
{
    return s1.size() < s2.size();
}
bool GT6(string s)
{
    return s.size() >= 6;
}
int main()
{
    vector<string> svec;
    ifstream gushi("./gushi.txt");
    string str;

    while(gushi >> str)
    {
        svec.push_back(str);
    }

    //1. 排序
    sort(svec.begin(), svec.end());
    //2. 删除重复
    vector<string>::iterator it = unique(svec.begin(), svec.end());
    svec.erase(it, svec.end());
    //stable_sort(svec.begin(), svec.end(), isShort);
    //3. 统计
    vector<string>::size_type wc = count_if(svec.begin(), svec.end(), GT6);

    cout << wc << endl;
}

