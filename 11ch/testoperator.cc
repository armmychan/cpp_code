#include <iostream>

using namespace std;


class B{
    int j;
public:
    B(int j = 0):j(j){}
    int showJ(){
        return j;
    }
};

class A{
    int i;
public:
    A(int i = 0):i(i){}

    A & operator+(int val){
        i += val;
        return *this;
    }
    A & operator+(B & b){
        i += b.showJ();
        return *this;
    }

    void showI(){
        cout << i << endl;
    }
};

int main()
{
    A a;
    B b(5);
    a = a + 1;
    
    a.showI();

    a = a + b;
    a.showI();
    //(A(1) + B(2)).showI();
}

