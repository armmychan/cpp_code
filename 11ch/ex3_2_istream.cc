#include <iostream>
#include <fstream>
#include <iterator>
#include <vector>

using namespace std;
int main()
{
    ifstream gushi("./gushi.txt");
    istream_iterator<string> it(gushi);
    istream_iterator<string> end_of_stream;

    vector<string> svec(it, end_of_stream);

    for(vector<string>::iterator it = svec.begin(); it != svec.end(); ++it)
        cout << *it << ' ';
    cout << endl;

}

