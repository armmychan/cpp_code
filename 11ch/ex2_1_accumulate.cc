#include <iostream>
#include <numeric>
#include <vector>

using namespace std;
int main()
{
    vector<int> ivec;

    for(int i = 0; i < 10 ; ++i)
        ivec.push_back(i);
    cout << accumulate(ivec.begin(), ivec.end(), 10000) << endl; 

    vector<string> svec;

    for(int i = 0; i < 10; ++i)
        svec.push_back("hello");
    cout << accumulate(svec.begin(), svec.end(), string("")) << endl;
}
