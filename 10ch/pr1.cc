#include <iostream>
#include <vector>
#include <utility>

using namespace std;
int main()
{
    string str;
    int val;
    pair<string, int> p;
    vector<pair<string, int> > vp;

    while(cin >> str >> val)
    {
        //方法一
        p.first =str;
        p.second = val;
        //p->first = str;   //错误，p不是指针，所以不能用->
        //p->second = val;
        //方法二
        //p = pair<string, int>(str, val);
        //方法三
        //p = make_pair(str, val);
        //vector类型中存储的元素的副本
        vp.push_back(p);
    }

    for(vector<pair<string, int> >::iterator it = vp.begin(); it != vp.end(); ++it )
        cout << it->first << ' ' << it->second << endl;

    return 0;
}

