#include <iostream>
#include <vector>
#include <set>

using namespace std;
int main()
{
    vector<int> ivec;
    set<int> iset;

    for(int i = 0; i < 10; ++i)
    {
        ivec.push_back(i);
        ivec.push_back(i);
    }

    iset.insert(ivec.begin(), ivec.end());

    for(vector<int>::iterator it = ivec.begin(); it != ivec.end(); ++it)
        cout << *it << ' ';
    cout << endl;

    for(set<int>::iterator it = iset.begin(); it != iset.end(); ++it)
        cout << *it << ' ';
    cout << endl;

    return 0;

}

