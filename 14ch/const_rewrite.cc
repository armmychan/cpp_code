#include <iostream>

using namespace std;

void rewrite(const int i){
    cout << i << " const" << endl;
}

void rewrite(int i){
    cout << i << " no const" << endl;
}
int main()
{
    int i = 2;
    rewrite(1);
    rewrite(i);
}

