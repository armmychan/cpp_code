#include <iostream>

using namespace std;

class A{
    int a;
    int b;
public:
    A(int a = 0, int b = 0):a(a), b(b){}

    A operator+(const A & a2)const{
        return A(a+a2.a, b+a2.b);
    }
    friend ostream & operator<<(ostream & os, const A &a );
    friend ostream & operator<<(ostream & os, A &a );
};

ostream & operator<<(ostream & os, const A & a){
    cout<<a.a << ' ' << a.b;
}
ostream & operator<<(ostream & os,  A & a){
    cout<<a.b << ' ' << a.a;
}
int main()
{
    A a=A(1, 2);
    A b=A(3, 4);
    cout << a << endl;
    cout << a + b<< endl;
    cout << A(1, 2) << endl;
    
}

