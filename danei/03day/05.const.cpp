/*
 * mutable修饰的变量不受const约束
 * */
#include <iostream>

using namespace std;

class Array{
    char a[100];
    int len;
    //mutable修饰的变量不受const约束
    mutable int cur;
public:
    Array():cur(-1){}
    //在此函数中 cur不受const约束，因为cur有mutable约束
    //char first()const{cur = 0; return a[0];}
    char next()const{
        if(hasnext())
        {
            return a[++cur];
        }
    }
    bool hasnext() const{
        return cur != len-1;
    }

    void add(char c){
        a[len++] = c;
    }
    void add(const char * s){
        while(*s){
            add(*s++);
        }
    }
    void remove( int pos){
        while(pos < len){
            a[pos] = a[pos+1];
        }
        --len;
    }
    void show(){
        while(hasnext())
            cout << next() << ' ';
        cout << endl;
    }


    ~Array(){}
};

int main()
{
    Array a;
    a.add("hello");
    a.add("world");
    a.show();
}

