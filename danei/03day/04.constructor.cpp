/*
 * 尽量用引用，尽量用const
 * 默认构造函数 
 *  如果没定义任意构造函数，编译器会构造默认的无参构造函数，不做任何事情，如果有自定义构造函数，编译器不产生无参构造函数
 * 拷贝构造函数 
 *  如果没有拷贝构造函数，编译器会自动构造一个拷贝构造函数，功能是将对象逐个赋值
 * 析构函数 
 *  如果没有定义析构函数，编译器会生成一个析构函数，不做任何事情
 *
 *C++中会自动回收临时变量
 * */
#include <iostream>
#include <cstdlib>

using namespace std;

class F{
    int n;
    int d;
public:
    //默认构造函数
    F(int n = 0, int d = 1);
    //F(F f){cout << "F(F)";}   //ERROR,不断的调用自身
    //F(F& f){cout << "F(F)" << endl;}      //OK
    //F(const F& f){cout << "F(F)" << endl;}  //尽量用const
    //拷贝构造函数
    F(const F& f):n(f.n), d(f.d + 1){cout << "F(F)" << endl;}  //尽量用const
    void print(bool newline = true);
    void print(bool newline = true) const;
    ~F();
};

F::F(int n, int d):n(n), d(d){
    cout << this << ", F(" << n << "," << d << ")" << endl;
}
F::~F(){
    cout << this << ", ~F()" << n << '/' << d << endl;
}

void F::print(bool newline){
    cout <<"自由的" <<  n << '/' << d;
    if(newline) cout << endl;
}

void F::print(bool newline) const{
    cout << "只读的" << n << '/' << d;
    if(newline) cout << endl;
}

//这里会调用俩次构造函数，俩次析构函数，形参一次，返回类型一次
F func(F x){
    cout << endl;
    return x;
}

int main()
{
    F a(3, 5);
    cout << "******************\n";
    func(a);
    cout << "******************\n";
    F(3, 5);
    cout << "******************\n";
}
