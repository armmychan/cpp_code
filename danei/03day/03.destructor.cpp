/*
 * 类中的函数定义不在类体内，必须在类中声明；
 * const 对象只能调用有const修饰的成员
 * */
#include <iostream>
#include <cstdlib>

using namespace std;

class F{
    int n;
    int d;
public:
    F(int n = 0, int d = 1);
    //函数重载，优先调用没有const限制的
    void print(bool newline = true);
    void print(bool newline = true) const;  //const表示函数不能修改当前对象
    //可以人为调用析构函数，但是无意义
    ~F();
};

F::F(int n, int d):n(n), d(d){
    cout << this << ", F(" << n << "," << d << ")" << endl;
}
F::~F(){
    cout << this << ", ~F()" << n << '/' << d << endl;
}

void F::print(bool newline){
    cout <<"自由的" <<  n << '/' << d;
    if(newline) cout << endl;
}

//定义中必须要加const，不能省略
void F::print(bool newline) const{
    cout << "只读的" << n << '/' << d;
    if(newline) cout << endl;
}

int compare(const F& x, const F& y){
    x.print();  //只能调用用const修饰的print函数，如果没有const修饰的print函数则会报错
    y.print();
}


int main()
{
    F *p = new F;   //如果没有delete，动态分配的内存在程序结束的时候释放,不会调用析构函数
    F a(1, 2), b(3, 4);
    a.print();
    delete p; p = NULL;
    b.print();

    cout << "***********************************\n";
    F* q = static_cast<F*>(malloc(sizeof(F)));  //malloc分配的动态内存不会调用构造函数
    free(q);                                    //free不会调用析构函数
    cout << "***********************************\n";
    //创建和释放对象的顺序相反
    p = new F[5];
    delete[] p;
    cout << "***********************************\n";
    const F c(5, 6);
    c.print();
    cout << "***********************************\n";
    compare(a, b);
    cout << "***********************************\n";

}
