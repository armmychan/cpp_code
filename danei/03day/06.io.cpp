#include <iostream>
using namespace std;


class F{
    int n;
    int d;
public:
    F(int n = 0, int d = 1):n(n), d(d){};
    int getn(){return n;}
    int getd(){return d;}
    friend 
    istream& operator>>(istream& in, F& f);
};

istream& operator>>(istream& in, F& f){
    char c;
    in >>f.n >> c >> f.d;
}
ostream& operator<<(ostream& out, F& f){
    out << f.getn() << '/' << f.getd() << endl;
}
void operator>>(istream in, int n) {}
int main()
{
    F a, b;
    //cin >> b; //编译器会去寻找函数cin.operator>>(F)和operator>>(istream, F);
    int n;
    //cin >> n; //编译器会去寻找函数cin.opertor>>(int )  和operator>>(istream, int);而这里俩个函数都已经定义，会产生歧义，编译错误

    cin >> a >> b;
    cout << a << b;

}

