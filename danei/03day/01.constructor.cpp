#include <iostream>
#include <string>

using namespace std;

class Person{
    string name;
    bool gender;
    int age;
    Person* p;
public:
    Person(const char * n, bool b=true);
    void growup(int years);
    void show();
    void marry(Person& p);
};

Person::Person(const char * n, bool b/*=true*/){
    name = n;
    gender = b;
    age = 0;
    p = NULL;
}

void Person::growup(int years){
    age += years;
}

void Person::show(){
    cout << "大家好，我是" << (gender?"帅哥":"美女") << name << "，今年" << age << "岁。";
    if(p == NULL)
        cout << "目前单身." << endl;
    else
        cout << "爱人是" << p->name << endl;
}

void Person::marry(Person& p){
    //*(this->p) = p; //ERROR
    this->p = &p;
    p.p = this;
    cout << name << "和" << p.name << "结婚！\n";
}

int main()
{
    Person p1("芙蓉", false);
    Person p2("杨强", true);

    p1.growup(18);
    p2.growup(20);
    p1.show();
    p2.show();
    p2.marry(p1);
    p1.show();
    p2.show();

    Person c("二狗");

    c.growup(5);
    c.show();
}

