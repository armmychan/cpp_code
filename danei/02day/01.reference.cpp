/*
 * 引用做参数
 * */
#include <iostream>

using namespace std;

void JiaoHuan(int *a, int* b){int t = *a; *a = *b; *b = t;}
void JiaoHuan(int &a, int &b){int t = a; a = b; b = t;}
//void print(int &n)    //不能接受常量实参
//可以接受常量实参
void print(const int &n)
{
    cout <<&n << '\t' << hex << showbase << n << endl;
}

struct Window{
    string text;
    int x,y;
    int width, height;
};

void input(Window & w){
    cout << "请输入窗口标题、坐标、大小：\n";
    cin >> w.text >> w.x >> w.y >> w.width >> w.height;
}
void output(const Window & w){
    cout << "=============" << w.text << "============" << endl;
    cout << "从（" << w.x << "，" << w.y << "）到（" << w.x + w.width << "，" << w.y + w.height << "）" << endl;
}
int main()
{
    Window w;
    input(w);
    output(w);
    int m = 10, n = 20;
    cout << m << ',' << n << endl;
    JiaoHuan(&m, &n);
    cout << m << ',' << n << endl;
    JiaoHuan(m, n);
    cout << m << ',' << n << endl;
    //void (*p)(int &, int &) = JiaoHuan();
    void (*p)(int &, int &) = &JiaoHuan;
    p(m, n);
    cout << m << ',' << n << endl;

    cout << "&m = " << &m << endl;
    cout << "&n = " << &n << endl;

    print(m);
    print(n);

    //如果函数的形参为引用类型，必须用const修饰
    //
    print(123);
    print(m + n);

}

