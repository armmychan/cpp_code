#ifndef TIME_H
#define TIME_H 1

using namespace std;

class Time{
    int h;
    int m;
    int s;
public:
    Time();
    Time(int hour, int min, int sor);

    void show();
    void tick();
};
#endif

