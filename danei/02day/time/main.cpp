#include "time.h"
int main()
{
    //Time t1();    //这里编译器会当成是一个函数声明，返回的类型为Time
    Time t1;
    Time t2(23,59,59);
    t1.show();
    t2.show();

    t1.tick();
    t2.tick();
    t1.show();
    t2.show();
}

