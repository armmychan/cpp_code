#include <iostream>
#include "time.h"
#include <iomanip>

Time::Time():h(0), m(0), s(0){}
Time::Time(int hour, int min, int sor):h(hour), m(min), s(sor){}

void Time::show(){
    cout << setw(2) << setfill('0') << h << ':' << setw(2) << m << ':' << setw(2) << s << endl;
}
void Time::tick(){
    if(++s == 60){
        s = 0;
        if(++m == 60){
            m = 0;
            if(++h == 24){
                h = 0;
            }
        }
    }
}

