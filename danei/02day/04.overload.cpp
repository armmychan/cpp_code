/*
 * 声明和定义分开，默认值只能在声明中指定
 * 有默认实参的形参只能靠右放
 * 有默认实参的函数有可能值重载函数冲突
 * */
#include <iostream>

using namespace std;

void print(int a[], int n, char sep = ' ', bool bra = false);
void print(int a[], int n)
{
    print(a, n, ' ', false);
}

void print(int a[], int n, char sep)
{
    print(a, n, ',', false);
    cout << *a;
    for(int i = 1; i < n; ++i)
        cout <<sep <<  a[i];
    cout << endl;
}
void print(int a[], int n, char sep, bool bra)
{
    if(bra) cout << '[';
    cout << *a;
    for(int i = 1; i < n; ++i)
        cout <<sep <<  a[i];
    if(bra) cout << ']';
    cout << endl;

}

int main()
{
    int a[5] = {11, 22, 33, 44, 55};

    print(a, 5);
    print(a, 5, ',');
    print(a, 5, ',', true);

}
