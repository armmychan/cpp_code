#include <iostream>
#include <iomanip>

using namespace std;

class Time{
    int h;
    int m;
    int s;
public:
    Time():h(0), m(0), s(0){}
    Time(int hour, int min, int sor):h(hour), m(min), s(sor){}

    void show(){
        cout << setw(2) << setfill('0') << h << ':' << setw(2) << m << ':' << setw(2) << s << endl;
    }
    void tick(){
        if(++s == 60){
            s = 0;
            if(++m == 60){
                m = 0;
                if(++h == 24){
                    h = 0;
                }
            }
        };
    }
};

int main()
{
    //Time t1();    //这里编译器会当成是一个函数声明，返回的类型为Time
    Time t1;
    Time t2(23,59,59);
    t1.show();
    t2.show();

    t1.tick();
    t2.tick();
    t1.show();
    t2.show();
}

