#include <iostream>

using namespace std;
class A{
    int i;
public:
    A(){}
    void showI(){
        cout << i << endl;
    }

};

struct B{
    int i;
public:
    B(){}
    void showI(){
        cout << i << endl;
    }
};
int main()
{
    A a;
    a.showI();
    B b;
    b.showI();

}
