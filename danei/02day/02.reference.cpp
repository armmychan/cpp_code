/*
 * 引用做返回类型
 * */
#include <iostream>
using namespace std;

int& max(int& x, int& y){
    return x>y?x:y;
}

//这里返回的引用为引用的对象已经从内存中释放，编译会产生警告，不推荐这么做
int& counter()
{
    int cnt = 100;
    ++cnt;
    return cnt;
}
int main()
{
    int m = 10, n = 20;
    
    max(m, n) = 80;

    cout << n << endl;

    int x = counter();
    cout << x << endl;
}
