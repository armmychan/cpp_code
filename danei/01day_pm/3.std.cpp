#include <iostream> //input/output stream
#include <string>

int main()
{
    std::cout << "请输入您的姓名和年龄:\n";
    std::string name;
    int age;
    std::cin >> name >> age;
    std::cout << name << " 您好,您出生于" << 2012 - age << "年" << std::endl;
    return 0;   //可以不写，如果没有，编译器会自动加上。
}

