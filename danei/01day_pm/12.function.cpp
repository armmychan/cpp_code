#include <iostream>
using namespace std;

//在C++中函数没有形参，表示为void，而在C中则表示可以为任意参数
void f(){cout << "Hello" << endl;return;}
void f2(void){}
//void表示不返回任何值，但是可以返回一个函数
//void f2(void){return f();}
void f3(double){cout << "helo" << endl;}    //有类型，无名字：哑元
int main()
{
    //f(123);   //错误，f()表示函数个数为0；
    f();
    f2();
    f3(123.4);
}

