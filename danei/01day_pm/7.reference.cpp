#include <iostream>
using namespace std;

int main()
{
    double d = 123.45;
    double& e = d;  //引用必须初始化为一个变量的引用
    //double& f = 123.45 //ERROR, 必须初始化为一个变量的引用
    const double & c = 234.56;  //OK!

    cout << "&d = " << &d << '\n' << "&e = " << &e << endl;

    //int & n = d;  //ERROR
}

