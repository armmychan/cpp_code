/*
 * 如果使用gcc编译结果为123，如果使用g++编译，运行结果为100，因为C++做了大量的优化，认为用const修饰的变量n一直为100，遇到n时，直接使用100而不是再从改变了之后的内存中读取，如果需要读取，可以在定义的时候加上关键字volatile
 * volatile const int n = 100;
 * */
#include <stdio.h>

int main()
{
    const int n = 100;  //C++中，后面使用n的值的地方会直接用100代替
    volatile const int m = 200;//m随时可能改变，每次从内存重取
    int * p = (int*)&n;
    *p = 123;
    p = (int*)&m;
    *p = 123;


    printf("n= %d\t m = %d\n", n, m);

    return 0;
}
