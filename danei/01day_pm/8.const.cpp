#include <iostream>
using namespace std;

int main()
{
    const int n = 100;
    int * p = (int*)&n;
    *p = 123;

    cout << "n = " << n << ' ' << "&n = " << &n << endl;
    cout << "*p = " << *p << ' ' << "p = " << p << endl;

    return 0;
}
