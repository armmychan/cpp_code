#include <iostream>
#include <string>
#include <cstring>

using namespace std;
int main()
{
    //初始化
    string s3= "chan",s1 = "chan";
    char s2[100] = "chan";
    char s4[100] = "chan";
    //赋值
    s1 = "armmy";
    strcpy(s2, "armmy");
    //链接
    s1 += "plus";
    strcat(s2, "plus");
    //比较
    s1 == s3;
    strcmp(s2, s4);
    //获取长度
    s1.size();
    strlen(s2);
    //查找
    strchar(s2, 'a');
    strstr(s2, "a");
    s1.find();
    //访问元素
    s1[0];
    s2[0];
    //转换
    s1.c_str();//返回 const char * 类型
    

    return 0;
}

