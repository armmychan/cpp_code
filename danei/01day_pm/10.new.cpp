#include <iostream>
#include <cstdlib>

using namespace std;

int main()
{
    int * p = static_cast<int*>(malloc(sizeof(int)));
    int * q = new int;
    int * r = new int(100);
    int n = 10;
    int * a = new int[n];
    delete r; r = NULL;
    delete q; q = NULL;
    delete[] a; a = NULL;
    //delete p;//ERROR!
    free(p);
}
