#include <stdio.h>

int f();    //函数声明，在C++中必须有，在C中可有可无
int main()
{
    f();
    return 0;
}

int f(){
    printf("hello\n");
    return 0;
}
