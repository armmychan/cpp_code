#include <iostream> //input/output stream
#include <string>

using namespace std;
int main()
{
    cout << "请输入您的姓名和年龄:\n";
    string name;
    int age;
    cin >> name >> age;
    cout << name << " 您好,您出生于" << 2012 - age << "年" << endl;
    return 0;   //可以不写，如果没有，编译器会自动加上。
}

