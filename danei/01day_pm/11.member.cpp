#include <iostream>

using namespace std;

struct date{
    int year;
    int month;
    int day;
    void print(){
        cout << year << '-' << month << '-' << day << endl;
    }
};

void f(double d){ cout << d << endl;}

//输出指定成员的内容
void showmember(date * a, int n, int date::* mp){
    for(int i = 0; i < n; ++i)
        cout << a[i].*mp << ' ';
        //cout << *(a + i).*mp << ' ';  //ok
        //cout << (a+i)->*mp << ' ';    //ok
    cout << endl;
}
int main()
{
    date a[5] = {{2010, 8, 16}, {2010, 10, 1}, {2008, 8, 8}, {2012, 12, 20}, {1999, 12, 30}};
    date d = {1997, 7, 7};
    cout << "&d = " << &d << endl;
    cout << "&d.year = " << &d.year << ", &d.month = " << &d.month << ", &d.day = " << &d.day << endl;
    //函数和成员地址总是输出为 1 
    cout << &date::year << &date::month << &date::day << endl;
    cout << &main << &f << endl;

    union{
        int n;
        int date::* mp;
    };

    mp = &date::day;
    cout << "&date::day = " << n << endl;
    cout << d.*mp << endl;
    mp = &date::year;
    cout << d.*mp << endl;

    showmember(a, 5, &date::year);

    void (date::*pf)();
    pf = &date::print;
    (d.*pf)();
    (a->*pf)();

    
}

