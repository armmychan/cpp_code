#include <iostream>

using namespace std;

enum Course{UNIX, C, CPP, UC, VC};
int main()
{
    //enum Course c;
    //在c++中可以省略enum
    Course c;
    int n;
    c = CPP;
    n = CPP;

    //c = n;  //ERROR! 枚举类型为不同的类型
    return 0;
}

