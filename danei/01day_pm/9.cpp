/*
 * 类型转换：
 *  static_cast 数值类型之间转换，以及有一方类型为void*的指针之间的转换 --合理的转换
 *  const_cast  把常量转化为变量，用于临时去掉const/volatile限制
 *  reinterpret_cast    任意俩种指针类型之间，以及指针与数值类型之间
 *  dynamic_cast    
 * */
#include <iostream>
#include <cstdlib>
using namespace std;

int main()
{
    //int n = 45.67;    //会产生警告；
    int n = static_cast<int>(45.67);    //数值转换
    int * p = static_cast<int*>(calloc(sizeof(int), 10));   //指针转换
    const int k = n;
    cout << "k = " << k << "&k = "<< &k << "&n = " << &n << endl;
    const_cast<int&>(k) = 789;
    //转换之后，这里输出的的新的赋值，没有进行const优化直接用n替代，因为n为变量
    cout << "k = " << k << "&k = " << &k << endl;

    float f = 123.45f;
    p = reinterpret_cast<int*>(&f);
    cout << *p << endl;

    n = int(12.34);
    n = int();
    int m(100);
    int x();//错误,会被当作函数声明,而不是初始化为0
    cout << x << endl;  //输出1
    int(y) = 200;
    cout << "y = " << y << endl;

}

