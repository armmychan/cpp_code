#include <iostream>
using namespace std;
#include <string>
//i对象.getline(char数组,数组大小);如果不能读完一整行会设置错误状态
//getline(i对象,string对象);不是成员函数，二十全局函数
int main()
{
    char buf[10];
    if(!cin.getline(buf,sizeof(buf))){
        cout << "错误，行超长！" << endl;
        cin.clear();
        cin.ignore(1000,'\n');
    }

    string s;
    getline(cin, s);
    //getline(cin, s, '~');   //第三个参数为表示读到这个字符为止, 默认为'\n'
    cout << "buf = " << buf << endl;
    cout << "s = " << s << endl;
}
