/*
 * 构造函数 和 析构函数中没有多态
 * */
#include <iostream>
using namespace std;

class Animal{
public:
    // 构造函数 和 析构函数中没有多态
    Animal(){sleep();}
    virtual ~Animal(){shot();}
    //~Animal(){shot();}  //如果没有在析构函数前加virtual，则利用多态的时候会直接调用指针类型的类中的析构函数，而不调用实际对象的析构函数

    //这个函数没有使用virtual修饰，使用父类指针调用的时候，调用的是父类play函数,但是里面的语句却是转换成为this->eat()...所调用的还是想应对象中的函数
    void play(){
        eat();  //this->eat()   this是指针，会对this所指向的对象启用多态，调用相应类中的eat函数
        shot();
        sleep();
    }

    virtual void eat(){cout << "吃" << endl;}
    virtual void shot(){cout << "叫" << endl;}
    virtual void sleep(){cout << "睡" << endl;}
};

class Horse:public Animal{
public:
    Horse(){cout << "马" << endl;}
    ~Horse(){cout << "死马" << endl;}
    virtual void eat(){cout << "马吃" << endl;}
    virtual void shot(){cout << "马叫" << endl;}
    virtual void sleep(){cout << "马睡" << endl;}

};
class Tiger:public Animal{
public:
    Tiger(){cout << "虎" << endl;}
    ~Tiger(){cout << "死虎" << endl;}
    virtual void eat(){cout << "老虎吃" << endl;}
    virtual void shot(){cout << "老虎叫" << endl;}
    virtual void sleep(){cout << "老虎睡" << endl;}
};

int main()
{
    Animal *p = NULL;
    char choice;
    cout << "t --- Tiger; h --- Horse:";
    cin >> choice;
    if(choice == 'h')
        p = new Horse;
    else
        p = new Tiger;
    //注意这里调用的play函数并没有使用virtual修饰，但是结果还是与使用virtual修饰相同，为什么？
    p->play();
    p->Animal::play();
    cout << "===================" << endl;
    p->Animal::eat();
    cout << "===================" << endl;
    delete p;
    //Tiger ();

    return 0;
}

