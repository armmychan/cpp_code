/*
 * 非格式化io，不忽略空白字符
 * i.get(),o.put(), i.getline(),i.ignore(),i.putback(),i.peek()
 * */
#include <iostream>
using namespace std;

int main()
{
    //无形参，返回字符的ascii码或者EOF
    //int get()
    int n = cin.get();
    char c,d,e;
    //istream& get(char& ch)
    cin.get(c).get(d).get(e);
    cout << n << ',' << c << ',' << d << ',' << e << endl;

    //输出字符
    //ostream& put(const char& ch)
    cout.put(n).put(c).put(d).put(e);

    //第一个形参默认值为1，第二个默认参数为文件结束
    //忽略200个字符，直到遇到'\n'为止,包括'\n',
    cin.ignore(200,'\n');
    char ch;
    cin >> ch;
    cout << "ch = " << ch << endl;
}
