#include <iostream>
using namespace std;
#include <string>
#include <sstream>

int main()
{
    string s="12345 6.78 x hello 234 100";
    istringstream is(s);
    int a, b, c;
    double d;
    char e;
    char buf[100];
    is >> a >> d >> e >> buf >> oct >> b >> hex >> c;
    cout << "a=" << a
        << ", b=" << b
        << ", c=" << c
        << ", d=" << d
        << ", e=" << e
        << ", buf=" << buf << endl;
    cout << "=====================" << endl;
    ostringstream os;
    os << "a=" << a
        << ", b=" << b
        << ", c=" << c
        << ", d=" << d
        << ", e=" << e
        << ", buf=" << buf << endl;
    //返回C风格字符串
    cout << os.str() << endl;

}
