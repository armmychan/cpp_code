#include <iostream>
using namespace std;
//输出缓冲遇到换行、有输入、满、程序结束、flush会刷新缓存

int main()
{
    cout << "cout"; //有缓冲，可以重定向
    cerr << "cerr"; //无缓冲，不可重定向
    clog << "clog"; //理论有缓冲，实际无缓冲，不可重定向，与cerr一致
}

