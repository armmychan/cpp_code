/*
 * IO对象要求能转换成bool对象
 *  对处于正常状态的流转换为true
 *  对处于错误状态的流转换为false
 *  .eof(), .fail(),.bad(),.good()
 * */
#include <iostream>
using namespace std;

int main()
{
    cout << "cin = " << cin << endl;
    cout << cout << endl;

    int n;
    //如果输入为int类型，则cin正常
    //如果输入为其他类型，则cin错误
    //cin >> oct >> hex >> dec >> n
    cin >> n;
    cout << "cin=" << cin << endl;

    //处于错误状态的io对象，拒绝进行io操作，使用.clear()清除状态
    //clear()不清除缓冲，值清除错误状态
    if(!cin){cin.clear();}
    string str;
    cin >> str;
    cout << str << endl;
    //输出，进制
    //cin >> oct >> hex >> dec >> n
    //一旦指定，一直有效
    /*
    cout << hex << n << endl;
    cout << n << endl;
    cout << dec << n << endl;
    */
}
