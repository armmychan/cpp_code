#include <iostream>
using namespace std;

class A{
public:
    void f(){
        cout << "A::F()" << endl;
    }
};
class B:public A{
public:
    void f(){
        cout << "B::F()" << endl;
    }
};
int main()
{
    A a;
    B b;
    a.f();
    b.f();
}
