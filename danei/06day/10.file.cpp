#include <iostream>
using namespace std;
#include <string>
#include <fstream>

int main()
{
    string path="tt.cpp";
    //这里初始化只能接受C风格的字符串
    ifstream fin(path.c_str());
    if(!fin){
        cout << "fin ERROR" << endl;
        return 1;
    }

    ofstream fout("tt");
    if(!fout){
        cout << "fout ERROR" << endl;
        return 2;
    }
    char ch;
    while(fin.get(ch)){
        fout << ch;
    }
    /*
     *会多一个字符
    while(cin){
        cin.get();
        fout << ch;
    }
    */
    fin.close();
    fout.close();

    return 0;
}
