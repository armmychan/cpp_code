#include <iostream>
using namespace std;
//i.putback(字符)   退回put获取的字符
//i.peek()  //查看输入缓冲区的第一个字符，返回ascii码
int main()
{
    cin >> ws;  //跳过开始空白字符,对后面的空白无影响
    /*
    char c = cin.get();
    cin.putback(c);
    if(isdigit(c)){
        double d;
        cin >> d;
        cout << "d=" << d << endl;
    }else{
        string s;
        cin >> s;
        cout << "s=" << s << endl;
    }
    */
    if(cin.peek() > '0'&& cin.peek() < '9'){
        double d;
        cin >> d;
        cout << "d=" << d << endl;
    }else{
        string s;
        cin >> s;
        cout << "s=" << s << endl;
    }
}
