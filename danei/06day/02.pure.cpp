#include <iostream>
using namespace std;

//不允许创建抽象类的对象,但可以是用抽象的引用或者指针
class Animal{
public:
    //构造函数不能是虚函数
    Animal(){cout << "==========" << endl;}
    //析构函数一定不能为纯虚函数，因为析构函数必须要执行
    virtual ~Animal(){cout << "=============" << endl;}

    void play(){
        eat();
        shot();
        sleep();
    }
    //有纯虚函数的类成为抽象类
    //不允许创建抽象类的对象,但可以是用抽象的引用或者指针
    //纯虚函数, =0 表示该函数一定会在子类中覆盖
    virtual void eat()=0;
    virtual void shot()=0;
    virtual void sleep()=0;
};

class Horse:public Animal{
public:
    Horse(){cout << "马" << endl;}
    ~Horse(){cout << "死马" << endl;}
    virtual void eat(){cout << "马吃" << endl;}
    virtual void shot(){cout << "马叫" << endl;}
    virtual void sleep(){cout << "马睡" << endl;}

};
class Tiger:public Animal{
public:
    Tiger(){cout << "虎" << endl;}
    ~Tiger(){cout << "死虎" << endl;}
    virtual void eat(){cout << "老虎吃" << endl;}
    virtual void shot(){cout << "老虎叫" << endl;}
    virtual void sleep(){cout << "老虎睡" << endl;}
};

int main()
{
    Animal *p = NULL;
    char choice;
    cout << "t --- Tiger; h --- Horse:";
    cin >> choice;
    if(choice == 'h')
        p = new Horse;
    else
        p = new Tiger;
    p->play();
    p->Animal::play();
    cout << "===================" << endl;
    cout << "===================" << endl;
    delete p;

    return 0;
}

