#include <iostream>
#include <string>

using namespace std;

class Person{
protected:
    string name;
private:
    bool gender;
public:
    Person(const char * name, bool gender):name(name), gender(gender){}
    void eat(const char * p)const{
        cout <<name << "在吃" << p << endl;
    }
    void sleep()const;
    //虚函数
    virtual void show()const{
        cout << "大家好，我是" << (gender?"帅哥":"美女") << name << ", 很荣幸认识大家！" << endl;
    }
    const string& getname()const{ return name;}
};

class Teacher :public Person{
    string course;
public:
    void teach(const char * _class)const{
        cout << name << "在给" << _class << "讲" << course << "课程." << endl;
    }

    //这里的参数什么的必须和父类中的相同才能实现多态
    virtual void show()const{
        cout << "大家好，我是" << course << "老师" << name << endl;
    }
    Teacher(const char * name, bool gender, /*string&*/const char * course):Person(name, gender), course(course){}
};

int main()
{
    Person a("芙蓉", false);
    Teacher b("权哥", true, "C++");
    Person * p=NULL,*q=NULL;
    p = &a;
    q = &b;
    p->show();
    q->show();
    Person &c=b;
    c.show();
    //dynamic_cast检查父类对象是否为子类，如果是，返回子类指针，如果不是返回NULL
    cout << dynamic_cast<Teacher *>(p) << endl;
    cout << dynamic_cast<Teacher *>(q) << endl;

}

