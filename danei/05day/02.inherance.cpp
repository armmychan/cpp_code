/*
 * 子类的构造函数接收参数，但是不能直接初始化父类中的数据成员，只能通过父类的构造函数初始化，而且只能在初始化列表中调用父类的构造函数
 * 子类可以重写父类成员，重写之后，将父类同名成员函数(即使同名不同参)隐藏起来

    */
#include <iostream>
#include <string>

using namespace std;

class Person{
protected:
    string name;
private:
    bool gender;
public:
    Person(const char * name, bool gender):name(name), gender(gender){}
    void eat(const char * p)const{
        cout <<name << "在吃" << p << endl;
    }
    void sleep()const;
    void show()const{
        cout << "大家好，我是" << (gender?"帅哥":"美女") << name << ", 很荣幸认识大家！" << endl;
    }
    const string& getname()const{ return name;}
};

class Teacher :public Person{
    string course;
public:
    void teach(const char * _class)const{
        //cout << getname() << "在给" << _class << "讲" << course << "课程." << endl;
        cout << name << "在给" << _class << "讲" << course << "课程." << endl;
    }

    //子类可以重写父类成员，重写之后，将父类同名成员函数(即使同名不同参)隐藏起来
    void show(){
        cout << "大家好，我是" << course << "老师" << name << endl;
    }
    //子类的构造函数接收参数，但是不能直接初始化父类中的数据成员，只能通过父类的构造函数初始化，而且只能在初始化列表中调用父类的构造函数
    Teacher(const char * name, bool gender, /*string&*/const char * course):Person(name, gender), course(course){}
};

int main()
{
    Person a("芙蓉", false);
    Teacher b("权哥", true, "C++");
    a.eat("巧克力");
    b.eat("泡椒凤爪");
    a.show();

    //调用被隐藏的show函数
    b.Person::show();
    b.show();
    b.teach("1007班");

}

