#include <iostream>
using namespace std;

class Goods{
    double price;
public:
    //如果没有指定默认price的值，会出现编译错误，为什么
    Goods(double price = 12.3):price(price){cout << "Goods()" << endl;}
    double getPrice()const{return price;}
    ~Goods(){cout << "~Goods()" << endl;}
};
class Canmer:virtual public Goods{
public:
    void take(const char * obj)const{
        cout << "给" << obj << "照相" << endl;
    }
    Canmer(double d):Goods(d){cout << "Canmer()\n";}
    ~Canmer(){cout << "~Canmer()\n";}
};

class MP3:virtual public Goods{
public:
    void play(const char * song)const{
        cout << "播放《" << song << "》" << endl;
    }
    MP3(double d):Goods(d){cout << "MP3" << endl;}
    ~MP3(){cout << "~MP3" << endl;}
};

class Phone:virtual public Goods{
public:
    void dial(const char * no)const{
        cout << "拨打" << no << endl;
    }
    Phone(double d):Goods(d){cout << "Phone()" << endl;}
    ~Phone(){cout << "~Phone()" << endl;}
};

class Mphone :public Phone{
    string fectory;
    Goods g;
    Canmer c;
    MP3 m;

public:
    void visitweb(const char * url)const{
        cout << "访问" << url << endl;
    }
    void take(const char * str){c.take(str);}
    void play(const char * song){m.play(song);}
    double getPrice(){return g.getPrice();}
    Mphone(const char * fectory = "", double d = 0):fectory(fectory), Phone(d), g(d), c(d), m(d)/*, fectory(fectory)*/{cout << "Mphone()" << endl;}
    ~Mphone(){cout << "~Mphone()" << endl;}
};
int main()
{
    Mphone mp("Nokia", 500);
    cout << mp.getPrice() << endl;
}

