/*
 * 父类构造函数的执行顺序为继承的顺序
 * */
#include <iostream>
using namespace std;

class Canmer{
public:
    void take(const char * obj)const{
        cout << "给" << obj << "照相" << endl;
    }
    Canmer(){cout << "Canmer()\n";}
    ~Canmer(){cout << "~Canmer()\n";}
};

class MP3{
public:
    void play(const char * song)const{
        cout << "播放《" << song << "》" << endl;
    }
    MP3(){cout << "MP3" << endl;}
    ~MP3(){cout << "~MP3" << endl;}
};

class Phone{
public:
    void dial(const char * no)const{
        cout << "拨打" << no << endl;
    }
    Phone(){cout << "Phone()" << endl;}
    ~Phone(){cout << "~Phone()" << endl;}
};

//父类构造函数的执行顺序为继承的顺序
class Mphone :public Canmer, public MP3, public Phone{
public:
    void visitweb(const char * url)const{
        cout << "访问" << url << endl;
    }
    Mphone(){cout << "Mphone()" << endl;}
    ~Mphone(){cout << "~Mphone()" << endl;}
};
int main()
{
    Mphone mp;
    mp.take("陈军军");
    mp.play("北京欢迎你");
    mp.dial("110");
    mp.visitweb("www.baidu.com");
}

