#include <iostream>

using namespace std;

class A{
public:
    A(){cout << "A()\n";}
    ~A(){cout << "~A()\n";}
    static void* operator new(size_t bites){ cout << "new A" << endl;}
    static void operator delete(void* p){ cout << "delete A" << endl;}
};
int main()
{
    A* a = new A;   //1.分配内存；2.执行构造函数
    delete a;       //1.执行析构函数；2. 分配内存
}
