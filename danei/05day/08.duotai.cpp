#include <iostream>

using namespace std;

class USB{
public:
    //使用virtual关键字开启多态，如果没有virtual修饰，则在子类中会覆盖掉，而不继承访问级别
    virtual void plugin(){
        cout << "插入USB设备!" << endl;
    }
    virtual void work(){
        cout << "USB设备在工作!" << endl;
    }
    virtual void stop(){
        cout << "停止USB设备" << endl;
    }
};

class USBDisk:public USB{
public:
    //子类重写父类的类，继承的父类的vvirtual和访问权限，在子类中可以不必声明
    void plugin(){
        cout << "插入USBDisk设备!" << endl;
    }
    void work(){
        cout << "读写USBDisk!" << endl;
    }
    void stop(){
        cout << "停止USB读写i!" << endl;
    }
};

class USBFan:public USB{
public:
    void plugin(){
        cout << "插入USB风扇！" << endl;
    }
    void work(){
        cout << "USB风扇转起来！" << endl;
    }
    void stop(){
        cout << "停止USB风扇i!" << endl;
    }
};

class Computer{
public:
    void usb(USB* u)const{
        u->plugin();
        u->work();
        u->stop();
    }
    void usb(USB& u)const{
        u.plugin();
        u.work();
        u.stop();
    }
};
int main()
{
    USB* u1 = new USBDisk, *u2 = new USBFan;
    Computer c;
    cout << "*****************" << endl;
    c.usb(u1);
    c.usb(u2);
    cout << "*****************" << endl;
    c.usb(*u1);
    c.usb(*u2);
    cout << "*****************" << endl;
    USBDisk ud;
    ud.plugin();
}

