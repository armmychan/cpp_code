#include <iostream>
#include <typeinfo>
using namespace std;

class A{
public:
    virtual void f(){}
};

class B:public A{};
class C:public B{};
class D:public A{};
int main()
{
    //使用类型名字
    cout << typeid(A).name() << endl;
    cout << typeid(B).name() << endl;
    //使用对象
    A* p1 = new A, *p2 = new B, *p3 = new C, *p4 = new D, *p5 = new B; 
    cout << typeid(*p1).name() << endl;
    cout << typeid(*p2).name() << endl;
    //使用==和before
    //判断俩个对象是不是相同类型
    cout << (typeid(*p2) == typeid(*p5)) << endl;
    //判断*p2是不是*p3的父类
    cout << typeid(*p2).before(typeid(*p3)) << endl;
    //type_info类型不能被创建,typeid返回type_info类型
    const type_info& t = typeid(*p4);
    cout << t.name() << endl;

}

