#include <iostream>
using namespace std;

class Goods{
    double price;
public:
    Goods(double price):price(price){cout << "Goods()" << endl;}
    double getPrice()const{return price;}
    ~Goods(){cout << "~Goods()" << endl;}
};
//虚继承
class Canmer:virtual public Goods{
public:
    void take(const char * obj)const{
        cout << "给" << obj << "照相" << endl;
    }
    Canmer(double d):Goods(d){cout << "Canmer()\n";}
    ~Canmer(){cout << "~Canmer()\n";}
};

class MP3:virtual public Goods{
public:
    void play(const char * song)const{
        cout << "播放《" << song << "》" << endl;
    }
    MP3(double d):Goods(d){cout << "MP3" << endl;}
    ~MP3(){cout << "~MP3" << endl;}
};

class Phone:virtual public Goods{
public:
    void dial(const char * no)const{
        cout << "拨打" << no << endl;
    }
    Phone(double d):Goods(d){cout << "Phone()" << endl;}
    ~Phone(){cout << "~Phone()" << endl;}
};

class Mphone :public Canmer, public MP3, public Phone{
    string fectory;
public:
    void visitweb(const char * url)const{
        cout << "访问" << url << endl;
    }
    //初始化列表顺序没有要求
    //Mphone(const char * fectory = "", double d = 0):fectory(fectory), Canmer(d), MP3(d), Phone(d)/*, fectory(fectory)*/{cout << "Mphone()" << endl;}
    //虚继承的父类构造函数参数，由底层子类直接传递
    Mphone(const char * fectory = "", double d = 0):fectory(fectory), Canmer(d), MP3(d), Phone(d), Goods(d)/*, fectory(fectory)*/{cout << "Mphone()" << endl;}
    ~Mphone(){cout << "~Mphone()" << endl;}
};
int main()
{
    Mphone mp("Nokia", 500);
    cout << mp.getPrice() << endl;
}

