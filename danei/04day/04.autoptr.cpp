#include <iostream>
#include <string>

using namespace std;

struct Date{
    int year;
    int month;
    int day;
};

struct Person{
    string name;
    int age;
    bool gender;
    Date birth;

    Person(){
        cout << "创建Person对象：" << this << endl;
    }
    ~Person(){
        cout << "销毁Person对象：" << this << endl;
    }
};

class autoptr{
    Person * p;
    static int cnt;
public:
    autoptr(Person * p):p(p){
        ++cnt;
    }
    autoptr(const autoptr& ptr):p(ptr.p){
        ++cnt;
    }
    //重载->操作符，返回类型为所指向的对象的指针
    Person * operator->(){
        return p;
    }
    Person& operator*(){
        return *p;
    }
    ~autoptr(){
        if(--cnt == 0)
            delete p;
    }
};
//静态成员在类外部分配空间
int autoptr::cnt = 0;

int main()
{
    autoptr a = new Person();
    a->age = 88;
    cout << a->age << endl;//a->age == a.optrator->()->age;
    cout << (*a).age << endl;
}

