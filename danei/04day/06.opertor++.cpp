#include <iostream>
using namespace std;

class F{
    int n;
    int d;
public:
    F(int nn = 0, int dd = 1):n(nn), d(dd){
    }
    friend ostream& operator<<(ostream& out, F f){
        out << f.n << '/' << f.d << endl;
        return out;
    }
    F& operator++(){
        n+=d;
        return *this;
    }
    F operator++(int){
        F f = *this;
        n+=d;
        return f;
    }

    friend F& operator--(F& f){
        f.n-=f.d;
        return f;
    }
    friend F operator--(F& f, int){
        F tf = f;
        f.n-=f.d;
        return tf;
    }
    //类型转换运算符不需要写返回类型
    operator double()const{
        return 1.0*n/d;
    }
    operator bool()const{
        return n/*!=0*/;
    }
};
int main()
{
    F f1(1,3), f2(2,3), f3(17, 3);
    cout << ++f1 << endl;
    cout << f2++ << endl;
    cout << --f3 << endl;
    cout << f3-- << endl;
    cout << double(F(1,2)) << endl;
    cout <<boolalpha<< bool(F(1,2)) << endl;
}

