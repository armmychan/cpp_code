#include <iostream>
using namespace std;

class A{

public:
    typedef unsigned int uint;
    enum Color{Red, Yellow};
private:
    int * p;
    int n;
public:
    A(uint n):p(new int[n]), n(n){}
    A(){delete p;}
    int& operator[](int n){
        return p[n];
    }
};
int main()
{
    A a(5);
    a[2] = 30;
    
    cout << a[2] << endl;
    //这里赋值需要注意，Red在类A中
    A::Color cl = A::Red;
    cout << cl << endl;
}

