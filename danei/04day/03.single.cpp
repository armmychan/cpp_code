#include <iostream>
using namespace std;

class F{
    int n;
    int d;
    void reduce(){
        int mcd = maxcd(n<0?-n:n,d);
        if(mcd!=1) n/=mcd, d/=mcd;
    }
public:
    F(int nn = 0, int dd = 1):n(nn), d(dd){
        if(d == 0) throw 0;
        if(d < 0) {
            d = -d;
            n = -n;
        }
        reduce();
        cout << "f(" << n << '/' << d << ")" << endl;
    }

    static int maxcd(int a, int b){
        if(a == 0) return b;
        return maxcd(b%a, a);
    }

    F operator~()const {
        return F(d, n);
    }
    friend bool operator!(const F& f){
        //return f.n == 0;
        return !f.n;
    }

    friend ostream& operator<<(ostream& out, const F& f);
};

ostream& operator<<(ostream& out, const F& f){
    cout << f.n << '/' << f.d;
    return out;
}
int main()
{
    F f1(6, 8);
    cout << "f1 = " << f1 << endl;
    cout << "~f1 = " << ~f1 << endl;    //f1.operator~()
    cout << "!f1 = " << !f1 << endl;    //operator!(F)

}
