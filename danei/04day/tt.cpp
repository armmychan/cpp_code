#include <iostream>

using namespace std;

class A{
    int a;
public:
    A(int a):a(a){}
    ~A(){}
    A operator+(A la){
        return A(a + la.a);
    }
    friend ostream& operator<<(ostream& o,const A& a){
        cout << a.a << endl;
        return o;
    }
};
int main()
{
    cout << A(1) + A(2) + A(3) << endl;
}

