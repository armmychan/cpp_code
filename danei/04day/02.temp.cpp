#include <iostream>

using namespace std;

class Teacher{
    string name;
    string sourse;
public:
    Teacher(const char * name, const char * sourse):name(name), sourse(sourse){
        cout << "招聘" << this->sourse << "老师" << this->name << endl;
    }
    Teacher(const Teacher & t):name(t.name), sourse(t.sourse){
        cout << "复制" << this->sourse << "老师" << this->name << endl;
    }
    ~Teacher(){
        cout << "辞退" << this->sourse << "老师" << this->name << endl;
    }
};
int main()
{
    cout << "****************\n";
    Teacher t1("陈宗权", "C++");// t2(t1);
    cout << "****************\n";
    Teacher t2 = t1;    //初始化
    cout << "****************\n";
    Teacher t3 = Teacher("杨强", "UC"); //临时变量初始化一个新对象，通常会被编译器优化为：使用创建临时变量的参数初始化新对象
    cout << "****************\n";
    t2 = t3;    //赋值
    cout << "****************\n";
    t2 = Teacher("杨薇", "咨询");   //赋值  临时对象会及时释放
    cout << "****************\n";
}

