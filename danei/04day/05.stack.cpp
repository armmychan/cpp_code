/*
 * 如果类中有指针和动态内存的存在，必须重写拷贝构造函数、赋值操作符、析构函数
 * */
#include <iostream>

using namespace std;

typedef string T;
class Stack{
    typedef unsigned int uint;
    T * men; 
    uint max;
    uint len;
public:
    Stack(Stack & e):men(new T[e.max]), max(e.max), len(e.len){
        for(uint i = 0; i < e.len; ++i)
            men[i] = e.men[i];
    }
    Stack(uint n):men(new T[n]), max(n), len(0){}
    uint max_size()const{return max;}
    uint size()const{return len;}
    Stack& push(const T& s){
        if(len == max - 1) throw 1;
        men[len++] = s;
        return *this;
    }
    T pop(){
        if(len == 0) throw 0;
        return men[--len];
    }

    void print(){
        for(uint i = 0; i < len; ++i)
            cout << men[i] << ' ';
        cout << endl;
    }
    Stack& operator=(Stack & e){
        if(this == &e) return *this;

        delete [] men;
        men = new T[e.max];
        len = e.len;
        max = e.max;
        for(int i = 0; i < len; ++i)
            men[i] = e.men[i];
        return *this;
    }
    ~Stack(){delete [] men;}
};
int main()
{
    Stack s1(5);
    s1.push("1").push("2").push("3").push("4");
    s1.print();
    Stack s2(s1);
    s2.print();
    Stack s3 = s2;
    s3.print();
}

