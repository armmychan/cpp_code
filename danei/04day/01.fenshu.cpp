/*
    //如果一个函数内部没有使用对象的数据，可以定义为static函数，该函数不属于任何对象
 *
 * */
#include <iostream>
using namespace std;

class F{
    int n;
    int d;
    //如果只是内部使用的函数，可以定义为私有的
    void reduce(){
        int mcd = maxcd(n<0?-n:n,d);
        if(mcd!=1) n/=mcd, d/=mcd;
    }
public:
    F(int nn = 0, int dd = 1):n(nn), d(dd){
        if(d == 0) throw 0;
        if(d < 0) {
            d = -d;
            n = -n;
        }
        reduce();
        cout << "f(" << n << '/' << d << ")" << endl;
    }

    //如果一个函数内部没有使用对象的数据，可以定义为static函数，该函数不属于任何对象，因此没有this指针
    static int maxcd(int a, int b){
        if(a == 0) return b;
        return maxcd(b%a, a);
    }

    //使用友元，可以让不是这个类的函数访问这个类的私有成员
    //友元可以在类体中定义，也可以在类体外定义，但是必须在类体中声明
    friend ostream& operator<<(ostream& out, const F& f);
    //这里的返回类型不带&，因为是要范围一个实体
    friend F operator+(const F& a, const F& b){
        return F(a.n*b.d + a.d * b.n, a.d*b.d);
    }
    //这个是属于对象的运算符定义，默认运算符左边的变量为this变量
    F operator*(const F& rf)const{
        return F(n*rf.n, d*rf.d);   //匿名对象,编译器会优化，直接创建成为返回对象
    }

};
//ostream& operator<<(ostream& out, F& f){  //这里的第二个形参，没有使用const修饰，则第二个参数不能为临时变量
ostream& operator<<(ostream& out, const F& f){
    cout << f.n << '/' << f.d;
    return out;
}
int main()
{
    F f1(6, 8);
    F f2(-6, 9);
    F f3(6, -12);
    cout << f1 <<", " << f2 << ", " << f3 << endl; 
    //static函数的使用,在编译的时候会将f1.maxcd(...)变成F::maxcd(f, ...)
    cout << f1.maxcd(100, 256) << ' ' << F::maxcd(100, 256) << endl;

    //这里f1+f2返回的是一个临时变量
    cout << f1 + f2 << ',' << f1 + f3 << ',' << f2 + f3 << endl;
    cout << f1+f2+f3 << endl;

    cout << f1*f2<< endl;
}

